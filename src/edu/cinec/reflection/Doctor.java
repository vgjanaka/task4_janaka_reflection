package edu.cinec.reflection;

/**
 * @author Janaka Pathma Kumara
 */
public class Doctor {

    private String fastName;
    public String lastName;
    private int age;
    public double height;
    private double weight;

    public Doctor(String fastName, String lastName, int age, double height, double weight) {
        this.fastName = fastName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
    }

    private double calculateBMI() {
        double BMI = weight / (height * height);
        return BMI;
    }

    public String getFastName() {
        return fastName;
    }

    public void setFastName(String fastName) {
        this.fastName = fastName;
    }

    private String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String toString() {
        return String.format("(age:%d)", age);
    }
}
