package edu.cinec.reflection;

public class Reflection03 {

    public static void main(String[] args) {
        Doctor doctor = new Doctor("Ranga","Perera",33,1.5,55);
        System.out.println("class = " + doctor.getClass()); // get class
        System.out.println("class name = " + doctor.getClass().getName());  // get class name
    }
}
