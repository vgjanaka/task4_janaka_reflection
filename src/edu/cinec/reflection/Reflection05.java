package edu.cinec.reflection;

import java.lang.reflect.Field;

public class Reflection05 {

    public static void main(String[] args) throws IllegalAccessException {
        Doctor doctor = new Doctor("Ranga","Perera",33,1.5,55);
        Field[] fields = doctor.getClass().getDeclaredFields();
        System.out.printf("There are %d fields\n", fields.length);

        for (Field f : fields) {
            System.out.printf("field name=%s type=%s value=%d\n", f.getName(),
                    f.getType(), f.getInt(doctor));
        }
    }
}
