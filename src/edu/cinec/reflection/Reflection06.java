package edu.cinec.reflection;

import java.lang.reflect.Field;

public class Reflection06 {

    public static void main(String[] args) {
        Doctor doctor = new Doctor("Ranga","Perera",33,1.5,55);
        Field[] fields = doctor.getClass().getDeclaredFields();
        System.out.printf("There are %d fields\n", fields.length);

        for (Field f : fields) {
            System.out.printf("field name=%s type=%s accessible=%s\n", f.getName(),
                    f.getType(), f.isAccessible());
        }
    }
}
