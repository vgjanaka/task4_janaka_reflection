package edu.cinec.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Reflection10 {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Doctor doctor = new Doctor("Ranga", "Perera", 33, 1.5, 55);
        Method m = doctor.getClass().getDeclaredMethod("setAge", int.class);
        m.setAccessible(true);
        m.invoke(doctor, 76);
        System.out.println(doctor);
    }
}
